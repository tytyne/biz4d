<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Project;

class OwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $projectId = $request->segments()[1];
        $article = Project::findOrFail($projectId);

        if ($article->user_id !== $this->auth->getUser()->id) {
            abort(403, 'Unauthorized action.');
        }

        return $next($request);
    }
}
