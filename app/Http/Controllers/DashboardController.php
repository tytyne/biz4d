<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Project;
use DB;


class DashboardController extends Controller
{
    public function index(){

        $data = DB::table('projects')
        ->select(
         DB::raw('status as status'),
         DB::raw('count(*) as number'))
        ->groupBy('status')
        ->get();
      $array[] = ['Status', 'Number'];
      foreach($data as $key => $value)
      {
       $array[++$key] = [$value->status, $value->number];
      }
     
        if(Auth::user()->hasRole('user')){
            return view('userdash');
        }
        elseif(Auth::user()->hasRole('manager')){
            

            return view('managerdashboard')->with('status', json_encode($array));

        }
        elseif(Auth::user()->hasRole('superadmin')){
         
            return view('admindash')->with('status', json_encode($array));
        }
    }
    public function profile(){
        return view('myprofile');
    }
    public function newproject()
    {
    // Show create post form
    return view('projects.create');
    }
    // public function projects()
    // {
    //  return view('projects');
    // }
    public function myprojects(Project $project)
    { 

        
        // Get all Posts, ordered by the newest first
        $user = Auth::user();
        $projects = [];
        if($user->hasRole('user')){
            $projects =   Project::where("user_id",$user->id)->latest()->get();
        }else{

            $projects = Project::latest()->get();
        }
         

        // Pass Post Collection to view
        return view('projects.index', compact('projects'));
    }
    public function users(User $user)
    {
       
        // $users = User::latest()->get();
        $people = User::where('id', '!=', auth()->id())->latest()->get();
        
        // Pass user Collection to view
        return view('people.index',compact('people'));
    }
   
    public function changepassword()
    {
       
       
        
        // Pass user Collection to view
        return view('people.changingpassword');
    }

   
  
}
