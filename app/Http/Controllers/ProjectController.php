<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $user = Auth::user();
        $projects = [];
        if($user->hasRole('user')){
            $projects =   Project::where("user_id",$user->id)->latest()->get();
        }else{

            $projects = Project::latest()->get();
        }
         
        // Pass project Collection to view
        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Show create project form
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

          $request->validate([
            'name' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'video' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
            'desc' =>'required',
            'git' =>'required|url',
            'status' =>'required'
        ]);
        $project = $request->all();
      //check if file exist 
      $path = $request->file('image')->store('public');
      $pathv = $request->file('video')->store('public');
        $project = new Project;
        $project->name = $request->name;
        $project->image = $path;
        $project->desc = $request->desc;
        $project->video = $pathv;
        $project->git = $request->git;
        $project->status = $request->status;
        $user = $request->user();
       

        $user->projects()->save($project);
  
        
        // Redirect the user to the created project woth a success notification
        return redirect(route('projects.show', $project))->with('notification', 'project created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {

      
           // Pass current project to view
           return view('projects.show', compact('project'));
    }
  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {

       
        return view('projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {


        
        // Validate projected form data
        $validated = $request->validate([
            'name' => 'required',
            'desc' =>'required',
            'git' =>'required|url',
            'status' =>'required'
        ]);

        if($request->hasFile('image')){
            $request->validate([
              'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $path = $request->file('image')->store('public');
            $project->image = $path;
        }
        if($request->hasFile('video')){
            $request->validate([
                'video' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
            ]);
            $pathv = $request->file('video')->store('public');
            $project->video = $pathv;
        }
        $project->name = $request->name;
        $project->desc = $request->desc;
        $project->git = $request->git;
        $project->status = $request->status;


        // Update project with validated data
        $project->update($validated);

        // Redirect the user to the created project woth an updated notification
        return redirect(route('projects.show', $project))->with('notification', 'Project updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        // Delete the specified project
        $project->delete();

        // Redirect user with a deleted notification
        return redirect(route('projects.index'))->with('notification', '"' . $project->name .  '" deleted!');
    }
   
}
