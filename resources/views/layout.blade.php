<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') - Laravel Blog</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>

<body>
<x-app-layout>
    <x-slot name="header">
        <!-- <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard for profile') }}
        </h2> -->
    </x-slot>
    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navMenu">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>
        <div id="navMenu" class="navbar-menu">
            <div class="navbar-start">
                <a href="{{ route('projects.index') }}" class="navbar-item">
                    All Posts
                </a>
            </div>
            <div class="navbar-end">
                <div class="navbar-item">
                    
                @if (Auth::user()->hasRole('user'))
                    <div class="buttons">
                        <a href="{{ route('projects.create') }}" class="button is-info">
                        <strong>New Post</strong>
                    </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </nav>

    <section class="section">
        <div class="container">
            <div class="columns is-centered">
                <div class="column is-8">
                    @if (session('notification'))
                        <div class="notification is-primary">
                            {{ session('notification') }}
                        </div>
                    @endif
                    @yield('content')
                </div>
            </div>
        </div>
    </section>

    </x-app-layout>

    <script src="{{ asset('js/nav.js') }}"></script>

</body>

</html>