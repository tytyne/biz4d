<div class="content">

    <a href="">
        <h1 class="title">{{ $person->name }}</h1>
    </a>
    <h5>{{ $person->name }}</h5>
    <h5>{{ $person->email }}</h5>
    <h5>{{ $person->phone }}</h5>

    <form method="post" action="{{ route('people.destroy', $person) }}">
        @csrf
        @method('delete')
        <div class="field is-grouped">
        @if (Auth::user()->hasRole('superadmin'))
            <div class="control">
                <a href="{{ route('people.edit', $person) }}" class="button is-info is-outlined">
                    Edit
                </a>
            </div>
            @endif
            @if (Auth::user()->hasRole('superadmin'))
            <div class="control">
                <button type="submit" class="button is-danger is-outlined">Delete</button>
            </div>
            @endif
        </div>
    </form>

</div>