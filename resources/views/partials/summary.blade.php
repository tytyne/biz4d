<div class="content">

    <a href="">
        <h4 class="title">{{ $project->name }}</h4>
    </a>
    <p>{!! nl2br(e($project->desc)) !!}</p>
    <img src="{{ Storage::url($project->image) }}"  width="320" height="240"  alt="" /><br>
    <p>{!! nl2br(e($project->git)) !!}</p>
    <video width="320" height="240" controls>
    <source src="{{ Storage::url($project->video) }}" >
    </video><br>
    <p><b>status:</b> {{ $project->status }}</p>
    <p><b>Posted:</b> {{ $project->created_at->diffForHumans() }}</p>
    <form method="post" action="{{ route('projects.destroy', $project) }}">
        @csrf
        @method('delete')
        <div class="field is-grouped">
        @if (Auth::user()->hasRole('superadmin|user'))
            <div class="control">
                <a href="{{ route('projects.edit', $project) }}" class="button is-info is-outlined">
                    Edit
                </a>
            </div>
            @endif
            @if (Auth::user()->hasRole('superadmin'))
            <div class="control">
                <button type="submit" class="button is-danger is-outlined">Delete</button>
            </div>
            @endif
        </div>
        <br>
    </form>
    <br>

</div>