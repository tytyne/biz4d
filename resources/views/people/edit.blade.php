@section('title', 'Edit User')
@extends('layout')

@section('content')

<h1 class="title">Edit: {{ $person->name }}</h1>

<form method="post" action="{{ route('people.update', $person) }}">

    @csrf
    @method('patch')
    @include('partials.errors')

    <div class="field">
        <label class="label">Name</label>
        <div class="control">
            <input type="text" name="name" value="{{ $person->name }}" class="input" placeholder="name" minlength="5" maxlength="100" required />
        </div>
    </div>

    <div class="field">
        <label class="label">Phone</label>
        <div class="control">
            <input type="text" name="phone" value="{{ $person->phone }}" class="input" placeholder="Phone" minlength="5" maxlength="100" required />
        </div>
    </div>

  

    <div class="field">
        <div class="control">
            <button type="submit" class="button is-link is-outlined">Update</button>
        </div>
    </div>

</form>

@endsection