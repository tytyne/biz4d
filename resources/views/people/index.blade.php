@section('title', 'Home')
@extends('layout')

@section('content')
<div class="container">
<div class="row">
@foreach ($people as $person)
<div class="col-sm">
    @include('partials.customers')
</div>
@endforeach
</div>
</div>
@endsection