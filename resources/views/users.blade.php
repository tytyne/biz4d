<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard for super admin') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <i>this table shows list of users</i>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                </tr>
                    @foreach($values as $key => $value)
                <tr>
            
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->email }}</td>
                
                </tr>
                    @endforeach
           </table>

    
            </div>
            </div>
        </div>
    </div>
</x-app-layout>
