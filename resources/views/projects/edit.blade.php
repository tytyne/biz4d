@section('title', 'Edit project')
@section('action', route('projects.create'))
@extends('layout')

@section('content')

<h1 class="title">Edit: {{ $project->name }}</h1>

<form method="post" action="{{ route('projects.update', $project) }}">

    @csrf
    @method('patch')
    @include('partials.errors')
    <!-- project name -->
    <div class="field">
        <label class="label">Project name</label>
        <div class="control">
            <input type="text" name="name" value="{{ $project->name }}" class="input" placeholder="project name"  required />
        </div>
    </div>
    <!-- image -->
    <div class="field">
   
    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Post video:</strong>
                 <input type="file" name="image" class="form-control" placeholder="Post Title">
            </div>
            <div class="form-group">
              <img src="{{ Storage::url($project->image) }}" height="200" width="200" alt="" />


            </div>
        </div>
    <!-- video -->
    <div class="field">
   
   <div class="col-xs-12 col-sm-12 col-md-12">
           <div class="form-group">
               <strong>Post video:</strong>
                <input type="file" name="image" class="form-control" placeholder="Post Title">
           </div>
           <video width="320" height="240" controls>
            <source src="{{ Storage::url($project->video) }}" >
            </video>

           </div>
       </div>
    <!-- git link -->
    <div class="field">
        <label class="label">Title</label>
        <div class="control">
            <input type="text" name="git" value="{{ $project->git}}" class="input" placeholder="git link"  maxlength="100" required />
        </div>
    </div>
    <!-- description -->
    <div class="field">
        <label class="label">Project description</label>
        <div class="control">
            <textarea name="desc" class="textarea" placeholder="desc" minlength="5" maxlength="2000" required rows="10">{{ $project->desc}}</textarea>
        </div>
    </div>

    <div class="field">
        <label class="label">Status</label>
        <div class="control">
            <div class="select">
                <select name="status" required>
                    <option value="" disabled selected>Select status</option>
                    <option value="active" {{ $project->active === 'active' ? 'selected' : null }}>Active</option>
                    <option value="incomplete" {{ $project->incomplete === 'incomplete' ? 'selected' : null }}>Incomplete</option>
                    <option value="ongoing" {{ $project->ongoing === 'ongoing' ? 'selected' : null }}>Ongoing</option>
                    <option value="stuck" {{ $project->status === 'stuck' ? 'selected' : null }}>Stuck</option>
                </select>
            </div>
        </div>
    </div>

    <div class="field">
        <div class="control">
            <button type="submit" class="button is-link is-outlined">Update</button>
        </div>
    </div>

</form>

@endsection