@section('title', 'New Project')
@extends('layout')

@section('content')

<h1 class="title">Create a new project</h1>

<form method="post" action="{{ route('projects.store') }}" enctype="multipart/form-data">

    @csrf
    @include('partials.errors')
   <!-- project  name -->
    <div class="field">
        <label class="label">Project name</label>
        <div class="control">
            <input type="text" name="name" value="{{ old('name') }}" class="input" placeholder="project name" minlength="5" maxlength="100" required />
        </div>
    </div>
    <!-- image -->
    <div class="field">
   
    <strong>Project Image:</strong><br>
    <div class="control">
    <input type="file" name="image" class="form-control" placeholder="Post Title">
    </div>
    </div>
    <!-- video -->
    <div class="field">
    <strong>Project video:</strong>
    <div class="control">
        <input type="file" name="video" class="form-control" placeholder="Post Title">
    </div>
    </div>
      
    
    <!-- gitlink -->
    <div class="field">
        <label class="label">Git link</label>
        <div class="control">
            <input type="text" name="git" value="{{ old('git') }}" class="input" placeholder="git link" maxlength="100" required />
        </div>
    </div>

    <div class="field">
        <label class="label">description</label>
        <div class="control">
            <textarea name="desc" class="textarea" placeholder="desc" minlength="5" maxlength="2000" required rows="10">{{ old('desc') }}</textarea>
        </div>
    </div>

    <div class="field">
        <label class="label">Status</label>
        <div class="control">
            <div class="select">
                <select name="status" required>
                    <option value="" disabled selected>Select status</option>
                    <option value="active" {{ old('status') === 'active' ? 'selected' : null }}>Active</option>
                    <option value="incomplete" {{ old('status') === 'incomplete' ? 'selected' : null }}>Incomplete</option>
                    <option value="ongoing" {{ old('status') === 'ongoing' ? 'selected' : null }}>Ongoing</option>
                    <option value="stuck" {{ old('status') === 'stuck' ? 'selected' : null }}>Stuck</option>
                </select>
                
            </div>
        </div>
    </div>

    <div class="field">
        <div class="control">
            <button type="submit" class="button is-link is-outlined">Publish</button>
        </div>
    </div>

</form>

@endsection