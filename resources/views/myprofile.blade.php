
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') - Project poster</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
</head>

<body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard for profile') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                <section style="background-color: #eee;">
                <div class="container py-5">
                <div class="row">
                    <div class="col">
                    <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
                        <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item active" aria-current="page">User Profile</li>
                        </ol>
                    </nav>
                    </div>
                </div>

            <div class="row">
                <div class="col-lg-4">
                <div class="card mb-4">
                    <div class="card-body text-center">
                    <img src="https://mdbootstrap.com/img/Photos/new-templates/bootstrap-chat/ava3.png" alt="avatar" class="rounded-circle img-fluid" style="width: 150px; margin-left:90px;">
                    <h5 class="my-3">{{Auth::user()->name}}</h5>
                    </div>
                </div>
                
                </div>
                <div class="col-lg-8">
                <div class="card mb-4">
                    <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                        <p class="mb-0">Name</p>
                        </div>
                        <div class="col-sm-9">
                        <p class="text-muted mb-0"> {{Auth::user()->name}}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                        <p class="mb-0">Email</p>
                        </div>
                        <div class="col-sm-9">
                        <p class="text-muted mb-0"> {{Auth::user()->email}}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                        <p class="mb-0">Phone</p>
                        </div>
                        <div class="col-sm-9">
                        <p class="text-muted mb-0"> {{Auth::user()->phone}}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                        <p class="mb-0">Mobile</p>
                        </div>
                        <div class="col-sm-9">
                        <p class="text-muted mb-0"> {{Auth::user()->phone}}</p>
                        </div>
                    </div>
                    <hr>
                    </div>
                </div>
                
                </div>
            </div>
            </div>
        </section>
        </div>   
            </div>
        </div>
        
    </div>
</x-app-layout>
</body>
</html>