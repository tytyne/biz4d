<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

//auth route for both

Route::group(['middleware'=>['auth']],function(){
    Route::get('/dashboard','App\Http\controllers\DashboardController@index')->name
    ('dashboard');
    Route::get('/dashboard/myprofile','App\Http\controllers\DashboardController@profile')->name
    ('dashboard.myprofile');
});

Route::group(['middleware' => ['auth', 'role:user']], function() { 
    Route::get('/dashboard/newproject', 'App\Http\Controllers\DashboardController@newproject')->name('dashboard.newproject');
    Route::get('/dashboard/changepassword', 'App\Http\Controllers\DashboardController@changepassword')->name('dashboard.changepassword');
    Route::post('/change/password',   'App\Http\Controllers\UserController@changePassword')->name('profile.change.password');
});
Route::group(['middleware' => ['auth', 'role:manager|superadmin|user']], function() { 
   
    Route::get('/dashboard/myprojects', 'App\Http\Controllers\DashboardController@myprojects')->name('dashboard.myprojects');
});
Route::group(['middleware' => ['auth', 'role:manager|superadmin|user']], function() { 
    Route::get('/dashboard/users', 'App\Http\Controllers\DashboardController@users')->name('dash.users');

});


Route::resource('/dashboard/projects', 'App\Http\Controllers\ProjectController');

Route::resource('/dashboard/people', 'App\Http\Controllers\UserController');

require __DIR__.'/auth.php';
