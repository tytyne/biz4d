Project poster
=======

## Description
An application to help users to publish their projects.


## Tools Used
PHP : Language used.

javascript : second language used.

Laravel : PHP framework.

Github CI : Continuous Integration.


### Getting Started
These instructions will get you a copy of the project up and running on your local machine for development . See deployment for notes on how to deploy the project on a live system.

### Prerequisites to use of API


Any web browser

### Prerequisites to get this API running on your local system

1. Laravel/composer/Mysql DB

2. Any text editor(Preferrably VS Code)

3. Git

### Installation

1. Clone this repository into your local machine:
```bash
https://bitbucket.org/tytyne/biz4d
```
2. Install dependencies
```bash
-  composer install
```
3. change .env.example  to .env and put your credentials

4. generate a key for application
```bash
-  php artisan key:generate
```
5. generate tables to the database by running migrations
```bash
-  php artisan migrate
```
6. generate seeders (roles to the application)
```bash
-  php artisan db:seed
```
3. Start the application by running the start script
```bash
- php laravel serve
```
4. Install postman to test all endpoints on port 4000.



# Acknowledgement
<ul>
 <li> BIZ4D Company</li>
</ul>


