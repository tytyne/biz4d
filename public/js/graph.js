


google.charts.load('current', {'packages':['corechart']});

google.charts.setOnLoadCallback(drawChart);

function drawChart()
{
 var data = google.visualization.arrayToDataTable(analytics);
 var options = {
  title : 'Percentage status of projects'
 };
 var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
 chart.draw(data, options);
}
